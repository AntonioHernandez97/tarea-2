using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelloWorld : MonoBehaviour
{
    public GameObject prefaCube; 
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Instantiate(prefaCube); 
        Debug.Log("Updated for push!"); 
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Hello World Update"); 
    }
}
